
FROM alpine:3.6
LABEL Maintainer="Øyvind Nilsen <oyvind.nilsen@gmail.com>" \
      Description="Lightweight container with Nginx & PHP 7 based on Alpine Linux."

# Install packages
RUN apk update && \
    apk --no-cache add \
    php7 php7-fpm php7-json php7-openssl php7-curl \
    php7-zlib php7-xml php7-phar php7-intl php7-dom php7-xmlreader php7-ctype \
    php7-mbstring nginx supervisor curl

# Configure nginx
COPY config/nginx.conf /etc/nginx/nginx.conf

# Configure php
COPY config/fpm-pool.conf /etc/php7/php-fpm.d/zzz_custom.conf
COPY config/php.ini /etc/php7/conf.d/zzz_custom.ini

# Configure supervisor
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Copy code
RUN mkdir -p /var/www/html
#WORKDIR /var/www/html
COPY src/ /var/www/html/
RUN chmod -R 755 /var/www/html

# Ports
EXPOSE 80 443

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

